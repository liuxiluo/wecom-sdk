package cn.felord.wecom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WecomSdkApplication {

    public static void main(String[] args) {
        SpringApplication.run(WecomSdkApplication.class, args);
    }

}
