package cn.felord.domain.contactbook.user;

import lombok.Data;

import java.util.List;

/**
 * @author xiafang
 * @since 2023/6/12 9:39
 */
@Data
public class UserIds {
    private final List<String> useridList;
}
